# Phones comparison

Personnal comparison about mobile phones.

My personnal choice is focusing the OS first, thirdly the functionnalities/hardware and finaly the price.

- The OS choice will be the 5 I prefere.
- The minimum functionnalities/hardware are: NFC, Fingerprint reader, fluidity, 15+ hours autonomy. Bonus for front fingerprint reader, OLED screen, borderless and dual camera.
- Price should be less than 600€.

## Brands

Note: Not all brands are listed here. Only the most common ones.

- Huawei/Honor (same constructor)
- Apple
- Samsung
- OnePlus
- Xiaomi
- Google
- Sony

## OS of each one

| Constructor   | OS                    |
|---------------|-----------------------|
| Huawei/Honor  | EMUI                  |
| Apple         | iOS*                  |
| Samsung       | Samsung Experience    |
| OnePlus       | OxygenOS              |
| Xiaomi        | MIUI                  |
| Google        | Android Stock*        |
| Sony          | ZenUI                 |

*: No overlayer

## OS comparison

### EMUI (Honor/Huawei)

- Security Updates: - (~ 2 years)
- Design: +
- Fluidity: +
- Functionnalities: -
- Constructor apps: -

### iOS (Apple)

- Security Updates: +
- Design: +
- Fluidity: +
- Functionnalities: +
- Constructor apps: +

### Samsung Experience (Samsung)

- Security Updates: ?
- Design: -
- Fluidity: +
- Functionnalities: -
- Constructor apps: -

### OxygenOS (OnePlus)

- Security Updates: ?
- Design: +
- Fluidity: + 
- Functionnalities: +
- Constructor apps: ?

### MIUI (Xiaomi)

- Security Updates: ?
- Design: +
- Fluidity: ? 
- Functionnalities: +
- Constructor apps: ?

### Android Stock (Google)

- Security Updates: +
- Design: +
- Fluidity: +
- Functionnalities: +
- Constructor apps: +

### ZenUI (Sony)

- Security Updates: ?
- Design: ~
- Fluidity: +
- Functionnalities: +
- Constructor apps: -

### Overall

5 winners are:
- iOS
- Android Stock
- OxygenOS
- EMUI
- MIUI

## Phones that runs theses OS

Note: only the ones that linked to the price will be choosen, and availible on the official website.

### iOS

- Apple iPhone 7
- Apple iPhone 7 Plus
- Apple iPhone 8

### Android Stock

- Damn, I'm not crazy... at least 859€ for Pixel 3!

### OxygenOS

- OnePlus 6t

### EMUI

- Huawei P20 Pro
- Honor 9
- Honor 10
- Honor View20

### MIUI

- Xiaomi Mi Mix 3
- Xiaomi Mi 8
- Pocophone F1

### The 6 winners (arbitrary)

- Apple iPhone 7
- Apple iPhone 7 Plus
- Apple iPhone 8
- OnePlus 6t
- Huawei P20 Pro
- Honor View20

## Features/Harware

Goal reminder: `NFC, Fingerprint reader, fluidity, 15+ hours autonomy. Bonus for front fingerprint reader, OLED screen, borderless and dual camera.`

### Apple iPhone 7

- [x] NFC (2 pts)
- [x] Fingerprint reader (2 pts)
- [x] Fluidity (2 pts)
- [ ] 15+ hours autonomy (2 pts)
- [x] Front fingerprint reader (1 pt)
- [ ] OLED screen (1 pt)
- [ ] Borderless (1 pt)
- [ ] Dual camera (1 pt)

Total: 7pts

### Apple iPhone 7 Plus

- [x] NFC (2 pts)
- [x] Fingerprint reader (2 pts)
- [x] Fluidity (2 pts)
- [x] 15+ hours autonomy (2 pts)
- [x] Front fingerprint reader (1 pt)
- [ ] OLED screen (1 pt)
- [ ] Borderless (1 pt)
- [x] Dual camera (1 pt)

Total: 10pts

### Apple iPhone 8

- [x] NFC (2 pts)
- [x] Fingerprint reader (2 pts)
- [x] Fluidity (2 pts)
- [ ] 15+ hours autonomy (2 pts)
- [x] Front fingerprint reader (1 pt)
- [ ] OLED screen (1 pt)
- [ ] Borderless (1 pt)
- [ ] Dual camera (1 pt)

Total: 7pts

### OnePlus 6t

- [x] NFC (2 pts)
- [x] Fingerprint reader (2 pts)
- [x] Fluidity (2 pts)
- [x] 15+ hours autonomy (2 pts)
- [x] Front fingerprint reader (1 pt)
- [x] OLED screen (1 pt)
- [x] Borderless (1 pt)
- [x] Dual camera (1 pt)

Total: 12pts

### Huawei P20 Pro

- [x] NFC (2 pts)
- [x] Fingerprint reader (2 pts)
- [x] Fluidity (2 pts)
- [x] 15+ hours autonomy (2 pts)
- [x] Front fingerprint reader (1 pt)
- [x] OLED screen (1 pt)
- [ ] Borderless (1 pt)
- [x] Dual camera (1 pt)

Total: 11pts

### Honor View20

- [x] NFC (2 pts)
- [x] Fingerprint reader (2 pts)
- [ ] Fluidity (2 pts)
- [x] 15+ hours autonomy (2 pts)
- [ ] Front fingerprint reader (1 pt)
- [ ] OLED screen (1 pt)
- [x] Borderless (1 pt)
- [x] Dual camera (1 pt)

Total: 8pts

### Podium

#### 12pts (max)

- OnePlus 6t

#### 11pts

- Huawei P20 Pro

#### 10pts

- Apple iPhone 7 Plus

## The survivors

- OnePlus 6t (559€)
- Huawei P20 Pro (519€)
- Apple iPhone 7 Plus (529€)
